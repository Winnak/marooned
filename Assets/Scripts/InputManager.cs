﻿using UnityEngine;
using System.Collections.Generic;

public static class InputManager {
    [SerializeField()]
    public static Dictionary<string, KeyCode> Keys = new Dictionary<string, KeyCode>()
    {
        { "Fire1", KeyCode.Mouse0 },
        { "Fire2", KeyCode.Mouse1 },
        { "CursorLock", KeyCode.C },
        { "Jump", KeyCode.Space },
        { "Run", KeyCode.LeftShift },
        { "Interact", KeyCode.E },
        { "Pistol", KeyCode.Alpha1 },
        { "Melee", KeyCode.Alpha2 },
        { "Other", KeyCode.Alpha3 },
        { "Submit", KeyCode.Return },
        { "Cancel", KeyCode.Escape },
        { "Load", KeyCode.R },
        { "Inventory", KeyCode.I },
        { "Drop", KeyCode.K },
        { "Sucicide", KeyCode.H }
    };
}
