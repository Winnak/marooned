﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class RandomModelSelecter : MonoBehaviour 
{
    public List<GameObject> variations;

	void Start () 
    {
        if (variations != null)
        {
            this.GetComponent<MeshFilter>().sharedMesh 
                = variations[Random.Range(0, variations.Count)].GetComponent<MeshFilter>().sharedMesh;
        }
	}
}
