﻿using UnityEngine;
using System.Collections;

public enum Behaviour
{
	Idle, Patroling, Resting, Chasing
}

[RequireComponent(typeof(CharacterController))]
public class CrocodileAI : MonoBehaviour 
{
    public float maxEnergy = 23;
    public GameObject spawn;
    private const float patrolspeed = 300;
    private const float sprintspeed = 600;

    public float health = 100;

    private CharacterController controller;
    private GameObject target;
    private Vector3 moveDirection;
    private GameObject player;
	private float speed = 300;
	private float timer = 0;
    private float cooldown = 0;
    public float energy = 0;
    private float soundTimer = 0;
    private Behaviour state = Behaviour.Patroling;

    public AudioClip[] biteSounds = new AudioClip[3];
    public AudioClip[] idleSounds = new AudioClip[8];

    private bool biting = false;
	
	// Use this for initialization
	void Start () 
	{
		controller = GetComponent<CharacterController>();
		energy = maxEnergy;
        this.animation["Croc_Sprint"].speed = 2;
	}

    // Update is called once per frame
    void Update () 
    {
		if (GameObject.FindGameObjectWithTag("Player")) 
		{
			player = GameObject.FindGameObjectWithTag("Player");
		}
        cooldown -= Time.deltaTime;

        switch (state)
        {
            #region Idle
            default:
            case Behaviour.Idle:
                /// IDLE: 
                /// * Waits for a timer to expire before deciding on an action 
                /// * Will always chase if the player is close enough 

                speed = 0;

                // If has just performed an action
                if (cooldown < 0) 
                {
                    // If player is close by
                    if (Observe(25)) 
                    {
                        state = Behaviour.Chasing;
                    }
                }

                // If timer is up, find action
                if (timer < 0  )
                {

                     if (Random.value < 0.65f) 
                        {
                            // Start patrolling
                            this.state = Behaviour.Patroling;
                        }
                        else
                        {
                            // Try again next time
                            timer = Random.value * 6 + 2;
                        }
                }

                    // Play a random sound from the idleSound Array and resets the timer.
                if (soundTimer < 0 && !this.audio.isPlaying)
                {
                    int soundRoll = Random.Range(0, 8);
                    audio.PlayOneShot(idleSounds[soundRoll]);
                    soundTimer += Random.Range(10, 22);
                }

                soundTimer -= Time.deltaTime;
                timer -= Time.deltaTime;
                break;
            #endregion

            #region Patroling
            case Behaviour.Patroling:
                /// PATROLING:
                /// * Walks around
                /// * Picks a random point
                /// * Will always chase if the player is close enough 
                /// * When reached decides between picking a new or idle -> Idle/Patrol
                /// * If it gets kinda low on energy starts to rest -> Resting 
                
                if (target == null) 
                {
                    // TEMP:
                    target = new GameObject("TEMP - Waypoint");

                    target.transform.position = new Vector3(
                        spawn.transform.position.x + (10 - Random.value * 8) * 
                            spawn.GetComponent<CrocodileSpawner>().amount, 
                        0, 
                        spawn.transform.position.z + (10 - Random.value * 8) * 
                            spawn.GetComponent<CrocodileSpawner>().amount);

                    target.transform.parent = this.spawn.transform;
                }
                else 
                {
                    speed = patrolspeed;
                    energy -= Time.deltaTime * 0.7f;
                    moveDirection = (target.transform.position - this.transform.position).normalized;

                    // Rotate toward the player
                    var rot = Quaternion.LookRotation(
                        new Vector3(target.transform.position.x - this.transform.position.x,
                                0,
                                target.transform.position.z - this.transform.position.z));
                    
                    this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, rot, 2);
                }

                // If player is close by
                if (Observe(22)) 
                {
                    Destroy(target);
                    target = null;
                    state = Behaviour.Chasing;
                }

                // If target is reached
                else if (Vector3.Distance(this.transform.position.NUp(), target.transform.position.NUp()) < 2)
                {
                    timer = 0.3f;
                    this.state = Behaviour.Idle;
                    Destroy(target);
                    target = null;
                }

                if (energy / maxEnergy < 0.25f) 
                {
                    Destroy(target);
                    target = null;
                    this.state = Behaviour.Resting;
                }

                break;
            #endregion

            #region Resting
            case Behaviour.Resting:
                /// REST:
                /// * Will  chase if the player is close enough and has somewhat suffecient energy
                /// * Rests until full on energy
                 
                this.speed = 0;
                this.energy += 2 * Time.deltaTime;

                if (!Observe(3))
                    biting = false;

                if (energy >= maxEnergy) 
                {
                    this.energy = maxEnergy;
                    this.state = Behaviour.Idle;
                }

                // If player is close by
                if (Observe(7) && energy/maxEnergy > 0.4f) 
                {
                    this.state = Behaviour.Chasing;
                }
                break;

            #endregion

            #region Chasing
            case Behaviour.Chasing:
                /// CHASING: 
                /// * Follows the player
                /// * Bites if close enough -> Idle -> Chasing
                /// * Sprints if it is close enough 
                /// * Stops following if too far away -> Idle -> Return/Patrol
                /// * Stops following if out of energy -> Resting
                
                // If has just performed an action
                if (cooldown < 0) 
                {
                    // If energy is high enough sprint
                    if (energy/maxEnergy > 0.6f) 
                    {
                        this.energy -= 2 * Time.deltaTime;
                        this.speed = sprintspeed;
                    }
                    else 
                    {
                        this.energy -= Time.deltaTime;
                        this.speed = patrolspeed;
                    }

                    // Chasing
                    moveDirection = (player.transform.position - this.transform.position).normalized;

                    // Rotate toward the player
                    var rot = Quaternion.LookRotation(
                        new Vector3(player.transform.position.x - this.transform.position.x,
                            0,
                            player.transform.position.z - this.transform.position.z));

                   this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, rot, 2);

                    // Too far away
                    if (!Observe(50)) 
                    {
                        biting = false;
                        this.state = Behaviour.Idle;
                    }
                    // Close enough to bite
                    else if (Observe(3)) 
                    {

                        if (!biting)
                            this.animation.Play("Croc_Attack");
                        biting = true;

                        int soundRoll = Random.Range(0, 4);
                        audio.PlayOneShot(biteSounds[soundRoll]);
                        player.GetComponent<PlayerStatus>().currentHealth -= 30;
                        this.state = Behaviour.Idle;
                        this.cooldown = 2.4f; //Cool down before next attack
                    }
                }
                else 
                {
                    biting = false;
                    this.state = Behaviour.Idle;
                }
                break;
            #endregion
        }

        RaycastHit rayinfo;
        if (Physics.Raycast(this.transform.position, Vector3.down, out rayinfo, 3)) 
        {
            Vector3 proj = this.transform.forward - (Vector3.Dot(this.transform.forward, rayinfo.normal)) * rayinfo.normal;
            transform.rotation = Quaternion.LookRotation(proj, rayinfo.normal);
        }    

		if (speed == patrolspeed)
        {
            this.animation.CrossFade("Croc_Walk", 0.1f);
		}
        else if (speed > patrolspeed)
        {
            this.animation.CrossFade("Croc_Sprint", 0.2f);
        }

        else if(biting)
        {
            animation["Croc_Attack"].wrapMode = WrapMode.Once;
            if (Observe(3))
                this.animation.Play("Croc_Attack");
            if (cooldown < 0)
                biting = false;
        }
        else
        {
            this.animation.CrossFade("Croc_Idle", 0.1f);
        }

        // OBS: Kun en move per update per gameobject
        controller.SimpleMove(moveDirection * speed * Time.deltaTime);
    
        if (this.transform.position.y < -10)
        {
            this.health = 0;
        }

        if (this.health <= 0)
        {
            Debug.Log("Croc died:" + this.transform.position + " x " + this.transform.parent.position);
            Destroy(gameObject);
        }
    }

    bool Observe(float range) 
    {
		return Vector3.Distance(player.transform.position, this.transform.position) < range;
    }

    public void TakeDamage(int damage)
    {
        this.health -= damage;
    }
}
