﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CrocodileSpawner : MonoBehaviour 
{
    public GameObject crocodille;
    public List<GameObject> crocs;
	public Vector2 CrocRange;
	[HideInInspector]public int amount;

	void Awake()
	{
		amount = Random.Range((int)CrocRange.x, (int)CrocRange.y);
	}

    void Start()
    {
        for (int i = 0; i < amount; i++)
        {
            crocs = new List<GameObject>();
            var croc = (GameObject)
                GameObject.Instantiate(crocodille, 
                                       new Vector3(
                    this.transform.position.x + (10 - Random.value * 20) * amount, 
                    this.transform.position.y + 1, 
					this.transform.position.z + (10 - Random.value * 20) * amount), 
                                       Random.rotation);

            croc.GetComponent<CrocodileAI>().spawn = this.gameObject;
            croc.transform.parent = this.gameObject.transform;

            crocs.Add(croc);
        }
    }
}
