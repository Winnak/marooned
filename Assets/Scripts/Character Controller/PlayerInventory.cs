﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInventory : MonoBehaviour 
{
    //TODO: Mange af disse fields burde encapsuleres.

	private float readItemDistance = 3f;
	private RaycastHit playerTarget;
    private FPSWalkerEnhanced playerMotor;
	private PlayerStatus playerstatus;
    private InGameMenuScript gameMenu;
	
	// Inventory
	public List<GameObject> Slots = new List<GameObject>();
	private const int MaxCarryLoad = 7;
	public List<Item> onPlayerItems = new List<Item>();
    private int carryLoad = 0;
	
	//Temp inventory liste. onPlayerItems har ikke krævet funktionalitet
	public List<Item> Items = new List<Item>();
	public GameObject slots;
	//public Itemdatabase database;
	public int slotNumber;
	public Vector2 slotPos;
	public Rect slotRect;
    public bool toggleInventory = false;
    
    [SerializeField]private Texture slotTexture;
	private const int MaxSlotsInARow = 5;
	private int slotsLeftInCurrenRow;
	private int clickedIndex; // Ofte når funktioner hedder sådan noget som "-Index" giver det lidt et hint til den skal flyttes til et mere enkapsuleret scope. 
	private int selectedItem = -1; // Det her lyder også som noget der kunne encapsuleres
	private int craftSlot1 = -1; // og dtte
	private int craftSlot2 = -1; // og dtte
	private bool craft1Filled = false; // og dette
	private bool craft2Filled = false; // og dette
	private int craftResult = -1; // og dette
	private Event e;
	private string tempItemName; // Temp lyder også som noget der ikke behøves at være field
    private bool pickingUp = false;

	private string[,] craftRecipeArray = new string[3, 2]{
		{"CubeFriend", "Stick"},
		{"CubeFriend", "Stick"},
		{"CapsuleTest", "Campfire"}
	};

    void Start ()
    {
        playerMotor = GetComponentInParent<FPSWalkerEnhanced>();
		playerstatus = GetComponentInParent<PlayerStatus> ();
        gameMenu = GetComponentInParent<InGameMenuScript>();

    }


	void Update ()
    {
        if (Input.GetButtonDown("Inventory"))
        {
            if(gameMenu.inMenu == false)
            {
                toggleInventory = !toggleInventory;
                playerMotor.inMenu = this.toggleInventory;
            }
        }
    }

    void FixedUpdate()
    {
        CheckInteractable ();
    }

    void CheckInteractable()//Tjekker på det spilleren Focusere på (midt scærm) inde for en bestemt range. Vis objectet har Taget Interactable kan det samles op
    {
        if (Physics.Raycast (transform.position, transform.forward, out playerTarget, readItemDistance)) 
        {
            Debug.DrawRay(transform.position, transform.forward * readItemDistance, Color.red);

            if (playerTarget.collider.tag == "Interactable") 
            {
                Debug.DrawRay (transform.position, transform.forward * readItemDistance, Color.green);

                if (Input.GetButtonDown("Interact") && carryLoad < MaxCarryLoad) 
                {
                    //TODO: måske drop at lave det til en funktion her men i  
                    // stedet bare gør hvad du ellers havde tænkt dig at gøre her.
                    // På den måde kan du flytte "playerTarget" ind i method-
                    // scope i stedet for at have den i classscope
                    // Eller, Alternativt/den mindre løsning:
                    // Giv PickUpItem en parameter RayCastHitInfo, og så bare flyt
                    // "playerTarget" ind i method scoped og passere den til PickUpItem();
                    // -- Erik
					tempItemName = playerTarget.collider.gameObject.name;
                    pickingUp = true;
                    PickUpItem();
				}
            }
        }
    }

    public int GetRock()//finder Rocks i inventory til kast
    {
        int ableRocks = 0;
        for (int i = 0; i < onPlayerItems.Count; i++)
        {
            if (onPlayerItems[i].itemName == "Rock" || onPlayerItems[i].itemName == "Rock(Clone)")
            {
                ableRocks++;
            }
        }
        return ableRocks;
    }
    // Fjerner en rock fra inventory når den bliver kastet
    public void OnThrow()
    {
        for (int i = 0; i < onPlayerItems.Count; i++)
        {
            if (onPlayerItems[i].itemName == "Rock" || onPlayerItems[i].itemName == "Rock(Clone)")
            {
                carryLoad--;
                onPlayerItems.RemoveAt(i);
                break;
            }
        }
    }

    void PickUpItem()//når items bliver added til inventory, tjekker om det er gennem craft eller pickup, vis pickop påvirker det world opjectet
    {
        //TODO/Note: det ligner at de items der er her har samme funktionalitet herinde,
        // Måske kan dette løses med en Item Base Class (noget i den stil) 
        // i stedet for det her.
        // Altså:
        // ItemBaseClass item = playerTarget.collider.GetComponent<ItemBaseClass>();
        // animation.Play(item.animation);
        // item.AddToInventory(); eller CarryLoad++;
        // item.Blablabla();
        // Destroy(target.collider.gameobject)
        // -- Erik

        switch (tempItemName)
        {
            case "Campfire":
            case "Campfire(Clone)":
                if (pickingUp) 
                {
                    Destroy(playerTarget.collider.gameObject);
                }
                onPlayerItems.Add(Item.Preset.CampfireSpawner());
                carryLoad++;
                break;
            case "Banana":
            case "Banana(Clone)":
                if (pickingUp) 
                {
                    Destroy(playerTarget.collider.gameObject);
                }
                onPlayerItems.Add(Item.Preset.BananaSpawner());
                carryLoad++;
                break;
            case "Rock":
            case "Rock(Clone)":
                if (pickingUp) 
                {
                    Destroy(playerTarget.collider.gameObject);
                }
                onPlayerItems.Add(Item.Preset.RockSpawner());
                carryLoad++;
                break;
            case "Mushroom01":
            case "Mushroom01(Clone)":
                if (pickingUp) 
                {
                    Destroy(playerTarget.collider.gameObject);
                }
                onPlayerItems.Add(Item.Preset.MushroomSpawner());
                carryLoad++;
                break;
            case "Stick":
            case "Stick(Clone)":
                if (pickingUp) 
                {
                    Destroy(playerTarget.collider.gameObject);
                }
                onPlayerItems.Add(Item.Preset.StickSpawner());
                carryLoad++;
                break;
            case "Bush":
            case "Bush(Clone)":
            case "Berries":
            case "Berries(Clone)":
                if ((tempItemName == "Bush" || tempItemName == "Bush(Clone)") && playerTarget.collider.gameObject.GetComponent<Bush>().Berries) 
                {
                    playerTarget.collider.gameObject.GetComponent<Bush>().Berries = false;
                    onPlayerItems.Add(Item.Preset.BerrySpawner());
                }
                else if ((tempItemName == "Bush" || tempItemName == "Bush(Clone)") && !playerTarget.collider.gameObject.GetComponent<Bush>().Berries)
                {
                    onPlayerItems.Add(Item.Preset.StickSpawner());
                    Destroy(playerTarget.collider.gameObject);
                }
                carryLoad++;

                break;
            case "CapsuleTest":
         case "CapsuleTest(Clone)":
                if (pickingUp) 
                {
                Destroy(playerTarget.collider.gameObject);
                }
                onPlayerItems.Add(Item.Preset.CapsuleTestSpawner());
                carryLoad++;
                break;
            case "CubeFriend":
            case "CubeFriend(Clone)":
                if (pickingUp) 
                {
                    Destroy(playerTarget.collider.gameObject);
                }
                onPlayerItems.Add(Item.Preset.CubeFriendSpawner());
                carryLoad++;
                break;
        }
    }

    /// <summary>
    /// Drops the item.
    /// </summary>
    private void DropItem()
    {			
        // bruges til at smide items. fjerner dem fra inventory og instancer dem i world
        switch (onPlayerItems[selectedItem].itemName) //TODO: Check for item first
        {
            case "Campfire":
            case "Campfire(Clone)":
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("Campfire"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;
            case "Banana":
            case "Banana(Clone)":
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("Banana"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;
            case "Rock":
            case "Rock(Clone)":
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("Rock"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;
            case "Mushroom":
            case "mushroom(Clone)":
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("Mushroom01"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;
            case "Stick":
            case "Stick(Clone)"://TODO/ skal tage højde for bush
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("Stick"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;
            case "Berries":
            case "Berries(Clone)"://TODO/ skal tage højde for bush
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("Berrys"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;

            case "CapsuleTest":
            case "CapsuleTest(Clone)":
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("CapsuleTest"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;

            case "CubeFriend":
            case "CubeFriend(Clone)":
                onPlayerItems.RemoveAt (selectedItem);
                Instantiate (Resources.Load<GameObject> ("CubeFriend"), transform.position + transform.forward, Quaternion.identity);
                carryLoad--;
                break;



            default:
                break;

        }
        selectedItem = -1;
    }

	void OnGUI()
	{
		int newRow = 1;
		for (int i = 0; i < MaxCarryLoad; i++)
		{
			if (i % MaxSlotsInARow == 0)
			{
				newRow++;
			}
		}

		if (toggleInventory)
		{
			slotsLeftInCurrenRow = MaxCarryLoad;
			slotTexture = Resources.Load<Texture> ("SlotImage");
			int currentIndex = 0;

			for (int j = 1; j < newRow; j++)	//draw inventory
			{
				if (slotsLeftInCurrenRow < MaxSlotsInARow)
				{
                    // til sidste row i inventory vis antallet at slots ikke går op i 5
					for (int k = 1; k < slotsLeftInCurrenRow + 1; k++)
					{
						int x = Screen.width / 2 - (MaxSlotsInARow * slotTexture.width) / 2;
                        int y = Screen.height / 2 - (newRow * slotTexture.height) / 2;
						GUI.Box(new Rect(
                            x + (slotTexture.width * k - slotTexture.width), 
                            y + (slotTexture.height * j - slotTexture.height), 
                            slotTexture.width, slotTexture.height), 
                                slotTexture);

						if (onPlayerItems.Count > currentIndex)
							GUI.Box(new Rect(
                                x + (slotTexture.width * k - slotTexture.width), 
                                y + (slotTexture.height * j - slotTexture.height),
                                slotTexture.width, slotTexture.height), 
                                onPlayerItems[currentIndex].itemIcon);

						currentIndex++;
					}
				}
				else
				{
                    //tegner slots til alle row der går op i 5
					for (int k = 1; k < MaxSlotsInARow + 1; k++)
					{
                        int x = Screen.width / 2 - (MaxSlotsInARow * slotTexture.width) / 2;
                        int y = Screen.height / 2 - (newRow * slotTexture.height) / 2;
						GUI.Box(new Rect(
                            x + (slotTexture.width * k - slotTexture.width), 
                            y + (slotTexture.height * j - slotTexture.height), 
                            slotTexture.width, slotTexture.height), 
                                slotTexture);

						if (onPlayerItems.Count > currentIndex)
							GUI.Box(new Rect(
                                x + (slotTexture.width * k - slotTexture.width), 
                                y + (slotTexture.height * j - slotTexture.height),
                                slotTexture.width, slotTexture.height), 
                                    onPlayerItems[currentIndex].itemIcon);
						
                        currentIndex++;
 					}
				}

				slotsLeftInCurrenRow = slotsLeftInCurrenRow - MaxSlotsInARow;
			}

			slotsLeftInCurrenRow = MaxCarryLoad;
			slotTexture = Resources.Load<Texture> ("SlotImage");

			if (Input.GetButtonDown("Fire1")) //inventory interact, bruges til at finde det item spilleren trykker på
			{
				e = Event.current;
				clickedIndex = 0;
				for (int b = 0; b < newRow; b++)
				{
					if (slotsLeftInCurrenRow < MaxSlotsInARow)
					{
						for (int n = 0; n < slotsLeftInCurrenRow; n++)
						{			
                            int x = Screen.width / 2 - (MaxSlotsInARow * slotTexture.width) / 2;
                            int y = Screen.height / 2 - (newRow * slotTexture.height) / 2;

                            Rect slotRect = new Rect(
                                x + (slotTexture.width  * (n + 1) - slotTexture.width),
                                y + (slotTexture.height * (b + 1) - slotTexture.height),
                                slotTexture.width ,
                                slotTexture.height);

							if (slotRect.Contains(e.mousePosition))
							{
								//Debug.Log(clickedIndex);
								if (clickedIndex < carryLoad)
								{
									selectedItem = clickedIndex;
								}
							}

							clickedIndex++;
						}
					}
					else if (slotsLeftInCurrenRow > MaxSlotsInARow)
					{
						for (int n = 0; n < MaxSlotsInARow; n++)
						{			
                            int x = Screen.width / 2 - (MaxSlotsInARow * slotTexture.width) / 2;
                            int y = Screen.height / 2 - (newRow * slotTexture.height) / 2;
                            
                            Rect slotRect = new Rect(
                                x + (slotTexture.width  * (n + 1) - slotTexture.width),
                                y + (slotTexture.height * (b + 1) - slotTexture.height),
                                slotTexture.width,
                                slotTexture.height);
                            
                            if (slotRect.Contains(e.mousePosition))
                            {
								//Debug.Log(clickedIndex);
								if (clickedIndex < carryLoad)
								{
									selectedItem = clickedIndex;
									//CONSUME FUNCTION
								}
							}
							clickedIndex++;
						}
					}

					slotsLeftInCurrenRow = slotsLeftInCurrenRow - MaxSlotsInARow;
				}
			}
            //laver rectangles til craft og create knapperne
			Rect craftSlot1Rect = new Rect(Screen.width - 90, 140, slotTexture.width, slotTexture.height);
			Rect craftSlot2Rect = new Rect(Screen.width - slotTexture.width - 15, 140, slotTexture.width, slotTexture.height);
			Rect craftNewItem = new Rect(Screen.width - 90, 220 , 75, 25);
			Rect craftResaultNameRect = new Rect(Screen.width - 90, 180 , 75, 25);
	
			GUI.Box(craftSlot1Rect,slotTexture);
			GUI.Box(craftSlot2Rect, slotTexture);



    		GUI.Box(new Rect(Screen.width / 2 - 100, 30 , 200, 25), " Selectet Item is " + (selectedItem + 1));

			e = Event.current;

			if (!craft1Filled)
			{
				GUI.Box(craftSlot1Rect,slotTexture);
			}
			if (craft1Filled)
			{
				GUI.Box(craftSlot1Rect, onPlayerItems[craftSlot1].itemIcon);
			}
			
			if (craft2Filled)
			{
				GUI.Box(craftSlot2Rect, onPlayerItems[craftSlot2].itemIcon);
			}

			if (selectedItem > -1)
			{
                /// Consumer Items
				if (onPlayerItems[selectedItem].consumableItem)//Comsume
				{
					Rect consumeBut = new Rect ( Screen.width - 90, 30 , 75, 25);
					GUI.Box(consumeBut, "Consume");
					if (Input.GetButtonDown("Fire1") && consumeBut.Contains(e.mousePosition))
                    {
                        craftSlot1 = -1;
                        craftSlot2 = -1;
                        craft1Filled = false;
                        craft2Filled = false;
                        playerstatus.Consume(onPlayerItems[selectedItem].itemHunger, onPlayerItems[selectedItem].itemThirst);
                        carryLoad--;
                        onPlayerItems.RemoveAt(selectedItem);
                        selectedItem = -1;
					}
				}
                // laver crafting slots med icons
                if (selectedItem > -1 && onPlayerItems[selectedItem].usedInCrafting)// Add to crating
				{
					Rect btn_addMaterial = new Rect( Screen.width - 90, 65, 75, 25);
					GUI.Box(btn_addMaterial, "Material");
					if (Input.GetButtonDown("Fire1") && btn_addMaterial.Contains(e.mousePosition))
					{
						if (craftSlot1 == -1)
						{
							craftSlot1 = selectedItem;
							craft1Filled = true;
							selectedItem = -1;
                            Debug.Log("C1F true");
						}
                        else if (craftSlot2 == -1 && craftSlot1 > -1 && craftSlot1 != selectedItem)
                        {
                            craftSlot2 = selectedItem;
                            craft2Filled = true;
                            Debug.Log("C2F true");
                        }
                        else 
                        {
                            // Giv spiller en error med begge craft slots er fyldt   
                        }
                    }
                }

                // tjekker Crafting arrayet igennem for at se om de valgte items kan craftes til noget
                if (craft1Filled && craft2Filled)
                {
                    for (int v = 0; v < 2; v++) 
                    {
                        if (onPlayerItems[craftSlot1].itemName == craftRecipeArray[0,v ] && 
                            onPlayerItems[craftSlot2].itemName == craftRecipeArray[1,v])
                        {
                            craftResult = v;
                        }
                    }
                    GUI.Box(craftNewItem, "Create");
                    GUI.Box(craftResaultNameRect, craftRecipeArray[2,craftResult]);

                   
                    // Køres når der trykkes på Craft i inventory
                    if (Input.GetButtonDown("Fire1"))
                    {           
                        if (craftResult > -1 && craftNewItem.Contains(e.mousePosition)) 
                        {       
                            var a = onPlayerItems[craftSlot2];
                            var b = onPlayerItems[craftSlot1]; // FIXME
                            tempItemName = craftRecipeArray[2,craftResult];
                            carryLoad--;
                            carryLoad--;
                            pickingUp = false;
                            PickUpItem();
                            craft1Filled = false;
                            craft2Filled = false;
                            craftSlot1 = -1;
                            craftSlot2 = -1;
                            selectedItem = -1;

                            onPlayerItems.Remove(a);
                            onPlayerItems.Remove(b);
                            
                        }
                    }
                }
                // Sætter værdier og laver call når drop bliver trykket på
				Rect DropBut = new Rect( Screen.width - 90, 100, 75, 25);
				GUI.Box(DropBut, "Drop");
				if (Input.GetButtonDown("Fire1") && DropBut.Contains(e.mousePosition) && selectedItem != -1)
				{
					DropItem();
					
					craft1Filled = false;
                    craftSlot1 = -1;
					craft2Filled = false;
                    craftSlot2 = -1;
					selectedItem = -1;
				}
			}
        }
    }
	    
}

