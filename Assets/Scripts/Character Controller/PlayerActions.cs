﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;



public class PlayerActions : MonoBehaviour
{
    private Transform playerTrans;
	private RaycastHit playerTarget;
    public AudioClip soundOne;
    public AudioClip soundTwo;
    private PlayerAnimations animationManager;
    private bool suicide;
    private GameObject fader;

    // Combat
	private Pistol flintlock;
    private PlayerInventory inventory;
    private float pistolRayDistance = 100.0f; //The Pistol ray distance
    
    //throw
    public Rigidbody ThrowRock;
    private float spawnDistance = 1.0f;
    private bool canThrow;

    void Start () 
	{
		playerTrans = transform;

        flintlock = GetComponentInChildren<Pistol>();
        inventory = gameObject.GetComponent<PlayerInventory>();
        animationManager = GetComponentInChildren<PlayerAnimations>();

        suicide = false;
        canThrow = true;

        fader = GameObject.FindGameObjectWithTag("Fader");
	}

	void Update () 
	{
            if (!suicide && Screen.lockCursor)
            {
                if (Input.GetButtonDown("Fire1"))
                    PrimaryAttack();

                if (Input.GetButtonDown("Fire2"))
                    SecondaryAttack();

                if (Input.GetButtonDown("Load"))
                    flintlock.LoadedState();

                if (Input.GetButtonDown("Suicide"))
                    Suicide("Start");
            }

            if (suicide && Screen.lockCursor)
                Suicide("Running");
	}

    public bool GetSuicide()
    {
        return suicide;
    }

	/// <summary>
	/// Handle player's primary attack.
	/// </summary>
	public void PrimaryAttack()
	{
        bool fireState = flintlock.CanFire();

		if (fireState)
        {
            //play the shoot animation
            animationManager.Shoot();

            //Plays the gunfire sound
            audio.PlayOneShot(soundOne);

            //Shoot a ray out from the players position in the direction the players faces and check if it hits something
            if (Physics.Raycast(playerTrans.position, playerTrans.forward, out playerTarget, pistolRayDistance))
            {
                if (playerTarget.collider.tag == "Enemy")
                    playerTarget.collider.gameObject.GetComponent<CrocodileAI>().TakeDamage(100);
            }
        }
        else
        {
            audio.PlayOneShot(soundTwo);
        }
    }

	/// <summary>
	/// Handle player' secondary attack
	/// </summary>
	public void SecondaryAttack()
	{
        int rocks = inventory.GetRock();

        if (rocks > 0 && canThrow)
        {
            canThrow = false;

            //Play throw animation
            StartCoroutine(Throw());
            
            inventory.OnThrow();
		}
	}

	/// <summary>
	/// Handle player's suicide
	/// </summary>
	public void Suicide(string state)
	{
		if (state == "Start")
        {
            bool loaded = flintlock.GetLoadedState();
            if (loaded == true)
            {
                suicide = true;
                animationManager.Suicide(1);
            }
        }

        if (state == "Running")
        {
            animationManager.Suicide(2);

            if (Input.GetButtonDown("Fire1"))
            {
                //Plays the gunfire sound
                audio.PlayOneShot(soundOne);

                //Set players health to 0 and kills the player
                GetComponentInParent<PlayerStatus>().currentHealth = 0;

                //Screen insta black out
                fader.GetComponent<Screenfader>().suicide = true;
            }
            else if (Input.GetButtonDown("Fire2"))
            {
                suicide = false;
                animationManager.Suicide(3);
            }
        }

	}

    IEnumerator Throw()
    {
        animationManager.Throw();
        yield return new WaitForSeconds(1);

		//throw a rock
		GameObject.Instantiate(ThrowRock, playerTrans.position + spawnDistance * playerTrans.forward, playerTrans.rotation);

        yield return new WaitForSeconds(.6f);

        canThrow = true;
    }
}