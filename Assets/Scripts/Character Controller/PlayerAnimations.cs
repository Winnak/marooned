﻿using UnityEngine;
using System.Collections;

public class PlayerAnimations : MonoBehaviour 
{
    void Start () 
    {
        animation.wrapMode = WrapMode.Loop;

        animation["FireGun"].wrapMode       = WrapMode.Once;
        animation["LoadGun"].wrapMode       = WrapMode.Once;
        animation["Ani_Suicide_1"].wrapMode = WrapMode.Once;
        animation["Throw"].wrapMode         = WrapMode.Once;

        animation["FireGun"].layer       = 1;
        animation["LoadGun"].layer       = 1;
        animation["Throw"].layer         = 1;
        animation["Ani_Suicide_1"].layer = 3;
        animation["Ani_Suicide_2"].layer = 2;

        animation.Stop();
    }

    void Update () 
    {
        if (Input.GetButton("Vertical") || Input.GetButton("Horizontal"))
            animation.CrossFade("WalkCycle");
        else
            animation.CrossFade("Idle");
    }

    public void Shoot()
    {
        animation.CrossFade("FireGun");
    }

    public void Load(bool loadable)
    {
        if (loadable)
        {
            animation["LoadGun"].speed = 1;
            animation["LoadGun"].time = 0;
            animation.CrossFade("LoadGun");
        }
        else
        {
            animation["LoadGun"].speed = -1;
            animation["LoadGun"].time = animation["LoadGun"].length;
            animation.Play("LoadGun");
        }
    }

    public void Suicide(int suicidePart)
    {
        switch (suicidePart)
        {
            case 1:
                animation["Ani_Suicide_1"].speed = 1;
                animation["Ani_Suicide_1"].time = 0;
                animation.Play("Ani_Suicide_1");
                break;
            case 2:
                animation.CrossFade("Ani_Suicide_2");
                break;
            case 3:
                animation.Stop();
                animation["Ani_Suicide_1"].speed = -1;
                animation["Ani_Suicide_1"].time = animation["Ani_Suicide_1"].length;
                animation.Play("Ani_Suicide_1");
                break;
        }
    }

    public void Throw()
    {
        animation.Play("Throw");
    }
}
