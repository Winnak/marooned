using UnityEngine;
using System.Collections;

public class PlayerStatus : MonoBehaviour
{
    public bool isAlive;

    //Variabler til health.
    public float currentHealth = 100;
    private const float maxHealth = 100;

    //Variabler til energy
    public float currentEnergy = 800;
    private float currentMaxEnergy = 800;
    private const float maxEnergy = 800;

    //Variabler til dynamiske størrelser af bars.
    float barLength = 0.0f;

	[SerializeField]private GameObject fader;

    private FPSWalkerEnhanced playerMotor;

    private float timer;

    void Start()
    {
        isAlive = true;
        playerMotor = this.GetComponent<FPSWalkerEnhanced>();
        barLength = Screen.width / 8;
        fader = GameObject.FindGameObjectWithTag("Fader");
    }

    void Update()
    {
        //Health kontrol - Food & Thirst
        if (currentEnergy <= 0)
        {
            currentHealth -= Time.deltaTime / 2;
        }

        //Energy system kontrol.
        //Hvis spilleren har energy kan de sprinte
        if (currentEnergy >= 0)
        {
            //Tjekker om 'Sprint' er trykket
            if (Input.GetButton("Run"))
            {
                currentEnergy -= Time.deltaTime * 1.5f;
            }
        }
        //Sikrer at Energy ikke falder under 0 eller overstiger currentMaxEnergy
        if (currentEnergy <= 0)
        {
            currentEnergy = 0;
        }
        if (currentEnergy >= currentMaxEnergy)
        {
            currentEnergy = currentMaxEnergy;
        }

        if(currentHealth <= 0)
        {
            isAlive = false;
            timer += Time.deltaTime;
            if(timer >= 5)
            {
                playerMotor.inMenu = true;
                Screen.lockCursor = false;
                Application.LoadLevel("menu");
            }
        }

        currentEnergy -= Time.deltaTime;
    }

    void OnGUI()
    {
        if(isAlive)
        {
            //Ikoner
            GUI.Box(new Rect(5, 30, 50, 23), "Health");
            GUI.Box(new Rect(5, 55, 65, 23), "Energy");

            //Health - Hunger - Thirst bars
            GUI.Box(new Rect(55, 30, barLength, 23), currentHealth.ToString("0") + "/" + maxHealth);
            GUI.Box(new Rect(70, 55, barLength - 15, 23), currentEnergy.ToString("0") + "/" + currentMaxEnergy);

        }
        if(currentHealth <= 0)
        {
            fader.GetComponent<Screenfader>().isDead = true;
            GUI.Box(new Rect((Screen.width / 2) - barLength / 2, Screen.height / 2, barLength, 23), "GAME OVER");
            currentHealth = 0;
        }
    }

    public void Consume(int hunger, int thirst)
    {
        currentEnergy += hunger + thirst;
    }

}