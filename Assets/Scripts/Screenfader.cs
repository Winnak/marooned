﻿using UnityEngine;
using System.Collections;

public class Screenfader : MonoBehaviour
{
    public float fadeSpeed = 1.5f;
    public float suicideSpeed = 10.0f;
    public bool sceneStarting = true;
    public bool fadeOut = false;
    public bool isDead = false;
    public bool hasWon = false;
    public bool suicide = false;
    public GameObject fader;
    float barLength = 0.0f;
    private float timer;
    private GameObject playerMotor;

    void Awake()
    {
        guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);

    }

    void Start()
    {
        timer = 0f;
        playerMotor = GameObject.FindGameObjectWithTag("Player");
        sceneStarting = true;
        barLength = Screen.width / 8;

    }

    void Update()
    {
        if (sceneStarting)
        {
            StartScene();
        }
        if (fadeOut)
        {
            EndScene("erik_testscene1");
        }
        if (isDead || hasWon)
        {
            guiTexture.enabled = true;
            FadeToBlack();
        }
        if (suicide)
        {
            Suicide("menu");
        }
        if (hasWon)
        {
            timer += Time.deltaTime;
            if (timer >= 5)
            {
                Screen.lockCursor = false;
                Application.LoadLevel("menu");
            }
        }
    }

    void FadeToClear()
    {
        guiTexture.color = Color.Lerp(guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
    }

    public void FadeToBlack()
    {
        guiTexture.color = Color.Lerp(guiTexture.color, Color.black, fadeSpeed * Time.deltaTime);
    }

    public void SuicideToBlack()
    {
        guiTexture.color = Color.Lerp(guiTexture.color, Color.black, suicideSpeed * Time.deltaTime);
    }
    
    void StartScene()
    {
        FadeToClear();
        if (guiTexture.color.a <= 0.05f)
        {
            guiTexture.color = Color.clear;
            guiTexture.enabled = false;
            sceneStarting = false;
        }
    }

    public void EndScene(string level)
    {
        guiTexture.enabled = true;
        FadeToBlack();
        if (guiTexture.color.a > 0.90f)
        {
            Application.LoadLevel(level);
            fadeOut = false;
            sceneStarting = true;
        }
    }

    public void Suicide(string level)
    {
        guiTexture.enabled = true;
        SuicideToBlack();
        timer += Time.deltaTime;
        if (timer >= 4)
        {
            Application.LoadLevel(level);
            Screen.lockCursor = false;
            fadeOut = false;
            sceneStarting = true; 
        }
    }

    void OnGUI()
    {
        if (hasWon)
        {
            GUI.Box(new Rect((Screen.width / 2) - barLength / 2, Screen.height / 2, barLength + 20, 23), "A WINNER IS YOU");
        }
    }


}
