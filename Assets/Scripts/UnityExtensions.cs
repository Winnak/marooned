﻿using UnityEngine;
using System.Collections;

public static class UnityExtensions 
{
    /// <summary>
    /// Strips Vector3's y coordinate
    /// </summary>
    /// <returns>The stripped vector</returns>
    /// <param name="vec">Vector</param>
    public static Vector3 NUp(this Vector3 vec)
    {
        return new Vector3(vec.x, 0, vec.z);
    }
}
