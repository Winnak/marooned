﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(TerrainGenerator))]
public class TerrainFauna : MonoBehaviour 
{
    Terrain t;
    TerrainGenerator island;
	
	public Vector2 crocNestRange = new Vector2(1,  4);
	public GameObject CrocodileSpawner;

	private int crocNestAmount = 0;

	void Awake () 
	{
		crocNestAmount = Random.Range((int)crocNestRange.x, (int)crocNestRange.y);
        island = this.gameObject.GetComponent<TerrainGenerator>();
        t = this.gameObject.GetComponent<Terrain>();
    }

	void Start ()
	{
		for (int i = 0; i < crocNestAmount; i++) 
		{
            Vector3 tpoint = island.coast[Random.Range(0, island.coast.Count)];
            
            Vector3 pos = new Vector3(tpoint.z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                                      16, 
                                      tpoint.x * t.terrainData.size.z / t.terrainData.heightmapWidth) 
                + island.transform.position;

            Instantiate(CrocodileSpawner, pos, Quaternion.identity);
		}
	}
}
