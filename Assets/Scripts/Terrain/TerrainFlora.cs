﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(TerrainGenerator))]
public class TerrainFlora : MonoBehaviour {
    private Terrain t;
    private List<Vector3> grassland;
    private List<Vector3> coast;

    [SerializeField]private GameObject palmCollider;
    [SerializeField]private GameObject bush;
    [SerializeField]private GameObject bananaCollider;
    [SerializeField]private GameObject banana;
    [SerializeField]private GameObject mushroom;
    [SerializeField]private GameObject stone;
    
    [SerializeField]private Vector2 forrestRange = new Vector2(1000, 1500);
    [SerializeField]private Vector2 bananaTreeRange = new Vector2(800, 1500);
    [SerializeField]private Vector2 bananaRange = new Vector2(2,   80);
    [SerializeField]private Vector2 treeRange = new Vector2(10,  100);
    [SerializeField]private Vector2 bushRange = new Vector2(40,  100);
    [SerializeField]private Vector2 mushRange = new Vector2(5,    50);
    [SerializeField]private Vector2 stoneRange = new Vector2(40,  100);
    [SerializeField]private Vector2 clutterRange = new Vector2(4800,  9600);
    
    private int forrestAmount = 1000;
    private int bananaTreeAmount = 1000;
	private int bananaAmount = 20;
	private int treeAmount = 10;
	private int bushAmount = 10;
    private int mushAmount = 10;
    private int stoneAmount = 40;
    private int clutterAmount = 4800;

	void Awake()
	{
		this.t                = this.gameObject.GetComponent<Terrain>();
        this.grassland        = this.gameObject.GetComponent<TerrainGenerator>().grassland;
		this.coast            = this.gameObject.GetComponent<TerrainGenerator>().sandland;
        this.forrestAmount    = Random.Range((int)forrestRange.x, (int)forrestRange.y + 1);
        this.bananaTreeAmount = Random.Range((int)bananaTreeRange.x, (int)bananaTreeRange.y + 1);
        this.bananaAmount     = Random.Range((int)bananaRange.x, (int)bananaRange.y + 1);
		this.treeAmount       = Random.Range((int)treeRange.x, (int)treeRange.y + 1);
		this.bushAmount       = Random.Range((int)bushRange.x, (int)bushRange.y + 1);
        this.mushAmount       = Random.Range((int)mushRange.x, (int)mushRange.y + 1);
        this.stoneAmount      = Random.Range((int)stoneRange.x, (int)stoneRange.y + 1);
        this.clutterAmount       = Random.Range((int)clutterRange.x, (int)clutterRange.y + 1);
	}

	void Start()
	{
		t.terrainData.treeInstances = new TreeInstance[0];

        // Forrest
		for (int i = 0; i < grassland.Count; i++) 
		{
            float chance = (forrestAmount + bushAmount + mushAmount + bananaTreeAmount + clutterAmount) / (float)(grassland.Count - i);

			if (Random.value < chance) 
			{
                int type = Random.Range(1, forrestAmount + bushAmount + mushAmount + bananaTreeAmount + clutterAmount);

				// Trees
                if (type <= forrestAmount) 
                { 
                    var collider = (GameObject)GameObject.Instantiate(palmCollider, Vector3.zero, Quaternion.identity);
                    
                    collider.transform.position = new Vector3(
                        grassland[i].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                        grassland[i].y, 
                        grassland[i].x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position; 
                    
                    collider.transform.parent = this.gameObject.transform;

                    int proto = Random.Range(0, 12);
                    if (proto < 5) 
                        proto = 0;

                    TreeInstance tree = new TreeInstance()
                    {
                        prototypeIndex = proto,
                        color = Color.white,
                        heightScale = 1 + Random.Range(-0.7f, 0.1f),
                        widthScale = 1 + Random.Range(-0.3f, 0.3f),
                        lightmapColor = Color.white,
                        position = new Vector3(
                            grassland[i].z/t.terrainData.size.x * t.terrainData.size.z / t.terrainData.heightmapWidth, 
                            grassland[i].y/t.terrainData.size.y, 
                            grassland[i].x/t.terrainData.size.z * t.terrainData.size.x / t.terrainData.heightmapHeight)
                    };
                    
                    t.AddTreeInstance(tree);
                    forrestAmount -= 1;
				} 
				// Bushes
                else if (type <= forrestAmount + bushAmount) 
                {
                    
                    var go = (GameObject)GameObject.Instantiate(bush, Vector3.zero, Quaternion.identity);

                    go.transform.position = new Vector3(
                        grassland[i].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                        grassland[i].y, 
                        grassland[i].x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position; 

                    var berries = Random.Range(0.0f, 1.0f) >= 0.3f;

                    go.GetComponent<Bush>().Berries = berries;

                    go.transform.parent = this.gameObject.transform;
					
					bushAmount -= 1;
				}
				// Mushrooms
                else if (type <= forrestAmount + bushAmount + mushAmount) 
                {
                    var ban = (GameObject)Instantiate(mushroom, new Vector3(
                        grassland[i].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                        grassland[i].y + 0.09f, 
                        grassland[i].x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position,
                                                      Quaternion.identity);
                    
                    ban.transform.parent = collider.transform;

					mushAmount -= 1;
                }
                // Banana tree
                else if (type <= forrestAmount + bushAmount + mushAmount + bananaTreeAmount) 
                {
                    var collider = (GameObject)GameObject.Instantiate(bananaCollider, Vector3.zero, Quaternion.identity);
                    
                    collider.transform.position = new Vector3(
                        grassland[i].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                        grassland[i].y, 
                        grassland[i].x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position; 
                    
                    collider.transform.parent = this.gameObject.transform;
                    
                    type = Random.Range(5, 17);
                    TreeInstance tree = new TreeInstance()
                    {
                        prototypeIndex = type,
                        color          = Color.white,
                        heightScale    = 1,
                        widthScale     = Random.Range(0.8f, 1.5f),
                        lightmapColor = Color.white,
                        position = new Vector3(
                            grassland[i].z/t.terrainData.size.x * t.terrainData.size.z / t.terrainData.heightmapWidth, 
                            grassland[i].y/t.terrainData.size.y, 
                            grassland[i].x/t.terrainData.size.z * t.terrainData.size.x / t.terrainData.heightmapHeight)
                    };
                    
                    t.AddTreeInstance(tree);
                    bananaTreeAmount -= 1;

                    if (Random.value < ((bananaAmount) / (float)(bananaTreeAmount))) 
                    {
                        var ban = (GameObject)Instantiate(banana, new Vector3(
                            grassland[i].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                            grassland[i].y + 0.8f, 
                            grassland[i].x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position,
                            Quaternion.identity);

                        ban.transform.parent = collider.transform;

                        bananaAmount -= 1;
                    }
                }
                else if (type <= forrestAmount + bushAmount + mushAmount + bananaTreeAmount + clutterAmount) 
                {
                    type = Random.Range(17, 24);
                    TreeInstance tree = new TreeInstance()
                    {
                        prototypeIndex = type,
                        color          = Color.white,
                        heightScale    = Random.Range(2, 4.5f),
                        widthScale     = Random.Range(3, 4.5f),
                        lightmapColor = Color.white,
                        position = new Vector3(
                            grassland[i].z/t.terrainData.size.x * t.terrainData.size.z / t.terrainData.heightmapWidth, 
                            grassland[i].y/t.terrainData.size.y, 
                            grassland[i].x/t.terrainData.size.z * t.terrainData.size.x / t.terrainData.heightmapHeight)
                    };
                    
                    t.AddTreeInstance(tree);
                    clutterAmount -= 1;
                }
			}
		}
        
        //Coast
        for (int i = 0; i < coast.Count; i++) 
        {
            float chance = (treeAmount + stoneAmount) / (float)(coast.Count - i);
            
            if (Random.value < chance) 
            {
                int type = Random.Range(1, treeAmount + stoneAmount);
                
                // Trees
                if (type <= treeAmount) 
                { 
                    var collider = (GameObject)GameObject.Instantiate(palmCollider, Vector3.zero, Quaternion.identity);

                    collider.transform.position = new Vector3(
                        coast[i].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                        coast[i].y, 
                        coast[i].x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position; 

                    collider.transform.parent = this.gameObject.transform;

                    TreeInstance tree = new TreeInstance()
                    {
                        prototypeIndex = 0,
                        color          = Color.white,
                        heightScale    = Random.Range(1.0f, 1.5f),
                        widthScale     = Random.Range(1.0f, 1.5f),
                        lightmapColor  = Color.white,
                        position = new Vector3(
                            coast[i].z/t.terrainData.size.x * t.terrainData.size.z / t.terrainData.heightmapWidth, 
                            coast[i].y/t.terrainData.size.y, 
                            coast[i].x/t.terrainData.size.z * t.terrainData.size.x / t.terrainData.heightmapHeight)
                    };

                    t.AddTreeInstance(tree);
                    treeAmount -= 1;
                }
                else if (type <= treeAmount + stoneAmount) 
                {
                    var ston = (GameObject)Instantiate(stone, new Vector3(
                        coast[i].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
                        coast[i].y + 0.2f, 
                        coast[i].x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position,
                                                      Quaternion.identity);
                    
                    ston.transform.parent = collider.transform;
                    
                    stoneAmount -= 1;
                }
            }
        }
	}
}