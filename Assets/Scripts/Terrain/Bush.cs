﻿using UnityEngine;
using System.Collections;

public class Bush : MonoBehaviour 
{
    private bool full = true;
    [SerializeField] private Material withBerries;
    [SerializeField] private Material withoutBerries;

    public bool Berries 
    { 
        get { return full; } 
        set 
        { 
            full = value;

            if (full) this.renderer.sharedMaterial = withBerries; 
            else      this.renderer.sharedMaterial = withoutBerries;
        }
    }
}
