﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(Terrain))]
public class TerrainGenerator : MonoBehaviour 
{
	private Terrain t;

	//Perlin Noise things
    [SerializeField] private float scale = 5.0f;      // 5 has proved to be a great number
    [SerializeField] private float heightMult = 2.0f; // 2 has proved to be a good number
	private int size = 1024; // should probably be changed to the dynamic value, just du'no' know which.
	private float[,] noise;     // height of terrain

	//Island things
	private float[,,] splatmap; // Textures
	[SerializeField] private AnimationCurve textureBlending = AnimationCurve.Linear(0, 0, 1, 1); //blending
    [HideInInspector] public List<Vector3> land = new List<Vector3>();  // Spawn points
    [HideInInspector] public List<Vector3> grassland = new List<Vector3>();  // Spawn points
	[HideInInspector] public List<Vector3> sandland = new List<Vector3>();  // Spawn points
	[HideInInspector] public List<Vector3> coast = new List<Vector3>(); // Spawn points

	//Misc
	public GameObject player;

	void Awake()
	{
		this.t = this.gameObject.GetComponent<Terrain>();
		this.size = t.terrainData.heightmapResolution - 1;
		this.noise = new float[size, size];
		this.splatmap = new float[size, size, t.terrainData.alphamapLayers];
	}

	void Start () 
	{
		CreateHeightMap();

		Vector3 pos = new Vector3(coast[0].z * t.terrainData.size.x / t.terrainData.heightmapHeight, 
		                          coast[0].y * t.terrainData.size.y / t.terrainData.alphamapHeight, 
		                          coast[0].x * t.terrainData.size.z / t.terrainData.heightmapWidth) 
			+ transform.position;

		pos.y = t.SampleHeight(pos) + 1;

		GameObject.Instantiate(player, pos, Quaternion.identity);
	}

	void CreateHeightMap()
	{
		float seed = Random.Range(0, 10000) + Random.value; //If it goes higher than 10k it will look weird
		float half = size * 0.5f;
		
		if (seed > 9999) seed = 1000566; // 1/10k chance to get a really weird seed
		Debug.Log("Seed: " + seed);

		int y = 0;
		while (y < size) 
		{
			int x = 0;
			while (x < size) 
			{
				//Create perlin noise
				float xCoord = seed + (float)x / size * scale;
				float yCoord = seed + (float)y / size * scale;
				float sample = Mathf.PerlinNoise(xCoord, yCoord);

				// Adjust to make it into an island
				sample *= heightMult * ((half - Mathf.Abs(half - x)) * (half - Mathf.Abs(half - y)))/(size * size);

				// Set everything under water to sand
				splatmap[x, y, 1] = 0;
				splatmap[x, y, 0] = 1;

				if (sample * t.terrainData.size.y < 11.5f) 
				{ 
					//Lower seabed
					sample *= (sample * t.terrainData.size.y)/11.5f;
					sample *= (sample * t.terrainData.size.y)/11.5f;
				}
				else 
				{ 
					// Entity spawn points
					if (sample * t.terrainData.size.y < 11.55f) 
						coast.Add(new Vector3(x, sample * t.terrainData.size.y, y));

					// Entity spawn points
					if (sample * t.terrainData.size.y > 12)
						land.Add(new Vector3(x, sample * t.terrainData.size.y, y));
					
                    if (sample * t.terrainData.size.y > 15) 
					{  
						//Paint grass
						splatmap[x, y, 1] =     textureBlending.Evaluate((sample * t.terrainData.size.y - 15)/3);
						splatmap[x, y, 0] = 1 - textureBlending.Evaluate((sample * t.terrainData.size.y - 15)/3);

                        if (sample * t.terrainData.size.y > 17f) 
                            grassland.Add(new Vector3(x, sample * t.terrainData.size.y, y));
                        else
                            sandland.Add(new Vector3(x, sample * t.terrainData.size.y, y));
					}
                    else 
                    {
                        sandland.Add(new Vector3(x, sample * t.terrainData.size.y, y));
                    }
				}

				//Put into heightmap
				noise[x, y] = sample;

				x++;
			}

			y++;
		}
		t.terrainData.SetAlphamaps(0, 0, splatmap);
		t.terrainData.SetHeights(0, 0, noise);
	}
}
//	private List<Vector2> coast = new List<Vector2>(); // all coast lines
//
// Check if over the water
//if (Mathf.Round(sample * t.terrainData.size.y * 10) == 105) {
//	coast.Add(new Vector2(x, y));
//}
//
//	void ConnectCoasts()
//	{
//		List<GameObject> coaster = new List<GameObject>();
//		coaster.Add(coastobj[0]);
//		coastobj.RemoveAt(0);
//
//		bool keepchecking = true;
//
//		foreach (var undef in coastobj.ToArray()) {
//			keepchecking = false;
//
//			foreach (var def in coaster) {
//				if (Mathf.Abs(undef.transform.position.x - def.transform.position.x) < 20) {
//					if (Mathf.Abs(undef.transform.position.y - def.transform.position.y) < 20) {
//						coastobj.Remove(undef);
//						coaster.Add(undef);
//						keepchecking = true;
//						break;
//					}
//				}
//			}
//		}
//
//		Debug.Log(coaster.Count);
//	}

/////////// TEXTURE BLENDS
// Logorithmic texture blend
//					var val = Mathf.Clamp(Mathf.Log(2, sample * t.terrainData.size.y - 9.5f) + 0.1f, 0f, 1f);
// Linear texture blend
//					float val = Mathf.Clamp(sample * t.terrainData.size.y * 0.09f - 7, 0f, 1f);
// Polynomial texture blend 
//					float blend = Mathf.Clamp(sample * sample * t.terrainData.size.y * t.terrainData.size.y * 0.05f - 8.5f, 0f, 1f);

/////////// MOVE OFFSET
//new Vector3(y * t.terrainData.size.x / t.terrainData.heightmapHeight, 
//			  sample * t.terrainData.size.y, 
//			  x * t.terrainData.size.z / t.terrainData.heightmapWidth) + this.transform.position, 
//////////////////////
