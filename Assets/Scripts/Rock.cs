﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour {

    public float force;
    

	// Use this for initialization
	void Start () {
        if (gameObject.name == "ThrownRock" || gameObject.name == "ThrownRock(Clone)")
            rigidbody.AddForce(transform.forward * force);
        else
            gameObject.name = "Rock";
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (gameObject.name == "ThrownRock" || gameObject.name == "ThrownRock(Clone)")
        {
            if (collisionInfo.gameObject.tag == "Enemy")
            {
                collisionInfo.gameObject.GetComponent<CrocodileAI>().TakeDamage(RandomDmg());
            }
            gameObject.name = "Rock";
        }
    }

    private int RandomDmg()
    {
        int number = Random.Range(10, 30);
        return number;    }
}
