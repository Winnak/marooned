﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item 
{
    //Klasse som styrer variabler og typer som en item kan være.
    public string itemName;
    public Texture itemIcon;
    public int itemHunger;
    public int itemThirst;
	public bool consumableItem;
	public bool usedInCrafting;
	public bool weapon;

    public Item()
    {

    }

	private static Item preset;// singleton

	public static Item Preset// singleton
	{
		get{

			if (preset == null) 
			{
				preset = new Item();
			}
			return preset;
		}
	}

	private Item Itemcore()// generic Item alle items grunder i den
	{
		Item tempItemCore = new Item ();
		tempItemCore.itemName = itemName;
		tempItemCore.itemIcon = itemIcon;
		tempItemCore.itemHunger = itemHunger;
		tempItemCore.itemThirst = itemThirst;
		tempItemCore.consumableItem = consumableItem;
		tempItemCore.usedInCrafting = usedInCrafting;
		tempItemCore.weapon = weapon;

		return tempItemCore;

	}

	#region CubeFriend
	public Item CubeFriendSpawner()
	{
		Item newCubeFriend = Itemcore ();
		newCubeFriend.itemName = "CubeFriend";
		newCubeFriend.itemIcon = Resources.Load<Texture> ("A_Armor05");
		newCubeFriend.itemHunger = 0;
		newCubeFriend.itemThirst = 50;
		newCubeFriend.consumableItem = true;
		newCubeFriend.usedInCrafting = true;
		newCubeFriend.weapon = false;
		
		return newCubeFriend;
	}
	#endregion

	#region CapsuleTestTest
	public Item CapsuleTestSpawner()
	{
		Item newCapsuleTest = Itemcore ();
		newCapsuleTest.itemName = "CapsuleTest";
		newCapsuleTest.itemIcon = Resources.Load<Texture> ("A_Armor04");
		newCapsuleTest.itemHunger = 50;
		newCapsuleTest.itemThirst = 0;
		newCapsuleTest.consumableItem = true;
		newCapsuleTest.usedInCrafting = false;
		newCapsuleTest.weapon = false;

		return newCapsuleTest;
	}
	#endregion

    #region Berry
    public Item BerrySpawner()
    {
        Item newBerrys = Itemcore();
        newBerrys.itemName = "Berries";
        newBerrys.itemIcon = Resources.Load<Texture> ("Icon_berries");
        newBerrys.itemHunger = 30;
        newBerrys.itemThirst = 20;
        newBerrys.consumableItem = true;
        newBerrys.usedInCrafting = false;
        newBerrys.weapon = false;

        return newBerrys;
    }
    #endregion

    #region Mushroom
    public Item MushroomSpawner()
    {
        Item newMushroom = Itemcore();
        newMushroom.itemName = "Mushroom";
        newMushroom.itemIcon = Resources.Load<Texture> ("Icon_Mushroom");
        newMushroom.itemHunger = 35;
        newMushroom.itemThirst = 15;
        newMushroom.consumableItem = true;
        newMushroom.usedInCrafting = false;
        newMushroom.weapon = false;

        return newMushroom;
    }
    #endregion

    #region Campfire
    public Item CampfireSpawner()
    {
        Item newCampfire = Itemcore();
        newCampfire.itemName = "Campfire";
        newCampfire.itemIcon = Resources.Load<Texture> ("A_Armor05");
        newCampfire.itemHunger = 0;
        newCampfire.itemThirst = 0;
        newCampfire.consumableItem = false;
        newCampfire.usedInCrafting = false;
        newCampfire.weapon = false;
        
        return newCampfire;
    }
    #endregion

    #region Banana
    public Item BananaSpawner()
    {
        Item newBanana = Itemcore();
        newBanana.itemName = "Banana";
        newBanana.itemIcon = Resources.Load<Texture> ("A_Armor04");
        newBanana.itemHunger = 40;
        newBanana.itemThirst = 10;
        newBanana.consumableItem = true;
        newBanana.usedInCrafting = false;
        newBanana.weapon = false;
        
        return newBanana;
    }
    #endregion

    #region Rock
    public Item RockSpawner()
    {
        Item newRock = Itemcore();
        newRock.itemName = "Rock";
        newRock.itemIcon = Resources.Load<Texture> ("Icon_Rock");
        newRock.itemHunger = 0;
        newRock.itemThirst = 0;
        newRock.consumableItem = false;
        newRock.usedInCrafting = false;
        newRock.weapon = true;

        return newRock;
    }
    #endregion

    #region Stick
    public Item StickSpawner()
    {
        Item newStick = Itemcore();
        newStick.itemName = "Stick";
        newStick.itemIcon = Resources.Load<Texture> ("Icon_Stick");
        newStick.itemHunger = 0;
        newStick.itemThirst = 0;
        newStick.consumableItem = false;
        newStick.usedInCrafting = true;
        newStick.weapon = false;

        return newStick;
    }
    #endregion

}
