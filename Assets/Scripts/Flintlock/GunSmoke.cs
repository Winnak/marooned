﻿using UnityEngine;
using System.Collections;

public class GunSmoke : MonoBehaviour 
{
    private Pistol flintlock;
    private PlayerActions pAction;

	// Use this for initialization
	void Start () 
    {
        flintlock = GameObject.Find("Flintlock").GetComponent<Pistol>();
        pAction = GetComponentInParent<PlayerActions>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetButtonDown("Fire1"))
        {
            bool loaded = flintlock.GetLoadedState();
            bool suicide = pAction.GetSuicide();

            if (loaded && !suicide && Screen.lockCursor)
            {
                gameObject.particleSystem.Play();
            }
        }
	}
}
