﻿using UnityEngine;
using System.Collections;

public class Pistol : MonoBehaviour 
{
	private bool loadedState; //Loaded state of the pistol
    private int bullet; //The number of bullets you have
    private PlayerAnimations pAni;

	// Use this for initialization
	void Start () 
    {
		loadedState = false;
        bullet = 1;
        pAni = GetComponentInParent<PlayerAnimations>();
	}

    //Check and change the loadedState of the Pistol
    public void LoadedState()
    {
        if (!loadedState && bullet > 0)
        {
            pAni.Load(true); //play the load animation
            loadedState = true;
            bullet--;
        }
        else if (loadedState)
        {
            pAni.Load(false); //play the unload animation
            loadedState = false;
            bullet++;
        }
    }

    //Get the loadedState
	public bool GetLoadedState()
    {
        return loadedState;
    }

    public bool CanFire()
    {
        if (loadedState)
        {
            bullet--;
            loadedState = false;
            return true;
        }
        else
            return false;
    }
}
