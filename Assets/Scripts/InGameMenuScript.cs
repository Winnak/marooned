﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class InGameMenuScript : MonoBehaviour
{
    private PlayerInventory inventory;
    private FPSWalkerEnhanced playerMotor;
    public Texture btnStartGameTexture1;
    public bool settingsMenu = false;
    private string settings;
    public static float volumeLevel;
    public static int fullscreen; //default...
    public static string screenres; //default.
    public string curRes;
    public static float currentResSlider;
    public string resolution;
    public bool inMenu;
    
    
    // Use this for initialization
    void Start()
    {   
        inMenu = false;             
        inventory = GetComponentInChildren<PlayerInventory>();
        playerMotor = this.GetComponent<FPSWalkerEnhanced>();
        
        switch (PlayerPrefs.GetInt("fullscreen"))
        {
            case 1:
                fullscreen = 1;
                break;
            default:
                fullscreen = 0;
                break;
        }            
        
        switch (PlayerPrefs.GetString("screenres"))
        {
            case "1":
                curRes = "1";
                currentResSlider = 0.0f;
                break;
            case "2":
                curRes = "2";
                currentResSlider = 0.5f;
                break;
            case "3":
                curRes = "3";
                currentResSlider = 1.0f;
                break;
            default:
                curRes = "1";   
                currentResSlider = 0.0f;
                break;        
        }
        if(PlayerPrefs.HasKey("volumeLevel"))
        {
            volumeLevel = PlayerPrefs.GetFloat("volumeLevel");
        }
        else
        {
            volumeLevel = 0.5f;
        }
        AudioListener.volume = volumeLevel;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            inMenu = !inMenu;
            if (inMenu)
            {
                inventory.toggleInventory = false;
            }
            playerMotor.inMenu = inMenu;
        }
    }

    void FixedUpdate()
    {
        if (curRes != screenres)
        {
            switch (screenres)
            {
                case "1":
                    Screen.SetResolution(1366, 768, fullscreen == 1);
                    resolution = "1366x768";
                    break;                
                case "2":
                    Screen.SetResolution(1600, 900, fullscreen == 1);
                    resolution = "1600x900";
                    break;
                case "3":
                    Screen.SetResolution(1920, 1080, fullscreen == 1);
                    resolution = "1920x1080";
                    break;
            }
            curRes = screenres;
        }

        if (AudioListener.volume != volumeLevel)
        {
            AudioListener.volume = volumeLevel;
        }
        
        if (Screen.fullScreen != (InGameMenuScript.fullscreen == 1))
        {
            Screen.fullScreen = InGameMenuScript.fullscreen == 1;    
        }
    }
    
    void OnGUI()
    {
        if (!settingsMenu && inMenu)
        {
            
            //Background for menu.
            GUI.Box(new Rect((Screen.width / 2) - 75, Screen.height / 2 - 100, 150, 225), "");
            
            
            //Resume, Settings, Main Menu & Exit button is created.
            if (GUI.Button(new Rect((Screen.width / 2) - 30, Screen.height / 2 - 75, 60, 24), "Resume"))
            {
                playerMotor.inMenu = !playerMotor.inMenu;
                inMenu = false;
            }
            
            if (GUI.Button(new Rect((Screen.width / 2) - 35, Screen.height / 2 - 25, 70, 24), "Settings"))
            {
                settingsMenu = true;
            } 
            
            if (GUI.Button(new Rect((Screen.width / 2) - 40, Screen.height / 2 + 25, 80, 24), "Main Menu"))
            {
                Application.LoadLevel("menu");
            } 
            
            if (GUI.Button(new Rect((Screen.width / 2) - 30, Screen.height / 2 + 75, 60, 24), "Exit"))
            {
                Application.Quit();
            } 
            
        }
        
        
        //Settings menu for Ambience sound, Screen resolution and Fullscreen mode.
        if (settingsMenu == true)
        {
            GUI.Box(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300), "");
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300), "Sound");
            GUI.Label(new Rect(Screen.width / 2 - 145, Screen.height / 2 - 130, 300, 300), "Volume");
            GUI.Label(new Rect(Screen.width / 2 + 115, Screen.height / 2 - 130, 300, 300), ((int)(volumeLevel * 100)).ToString() + "%");
            volumeLevel = GUI.HorizontalSlider(new Rect(Screen.width / 2 - 80, Screen.height / 2 - 125, 190, 50), volumeLevel, 0.0f, 1.0f);
            
            //Video;
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 100, 300, 300), "Graphics");
            fullscreen = GUI.Toggle(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 80, 200, 30), fullscreen == 1, "Fullscreen") ? 1 : 0;
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 55, 300, 300), "Resolution");
            currentResSlider = GUI.HorizontalSlider(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 50, 180, 50), currentResSlider, 0.0f, 1.0f);
            GUI.Label(new Rect(Screen.width / 2 + 85, Screen.height / 2 - 55, 300, 300), resolution);
            if (currentResSlider <= 0.3f)
            {
                screenres = "1";
            } else if (currentResSlider > 0.3f && currentResSlider <= 0.6f)
            {
                screenres = "2";
            } else if (currentResSlider > 0.6f)
            {
                screenres = "3";
            }
            //Close button
            if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height / 2 + 110, 120, 30), "Close"))
            {
                settingsMenu = false;
                saveSettings(fullscreen, screenres, volumeLevel);
            }
        }
    }
    
    public static void saveSettings(int fullscreen, string screenres, float volumeLevel)
    {
        PlayerPrefs.SetInt("fullscreen", fullscreen);
        PlayerPrefs.SetString("screenres", screenres);
        PlayerPrefs.SetFloat("volumeLevel", volumeLevel);
    }
}

#if THISWASNOTAFUCKUP
using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class InGameMenuScript : MonoBehaviour
{
    private PlayerInventory inventory;
    private FPSWalkerEnhanced playerMotor;
    public Texture btnStartGameTexture1;
    public bool settingsMenu = false;
    private string settings;
    public static float volumeLevel; //Start muted.
    public static int fullscreen; //default...
    public static string screenres; //default.
    public string curRes;
    public static float currentResSlider;
    public string resolution;
    public bool inMenu;


    // Use this for initialization
    void Start()
    {   
        inMenu = false;             
        inventory = this.GetComponent<PlayerInventory>();
        playerMotor = this.GetComponent<FPSWalkerEnhanced>();
            
        switch (PlayerPrefs.GetInt("fullscreen"))
        {
            case 1:
                fullscreen = 1;
                break;
            default:
                fullscreen = 0;
                break;
        }            
                        
        switch (PlayerPrefs.GetString("screenres"))
        {
            case "1":
                curRes = "1";
                currentResSlider = 0.0f;
                break;
            case "2":
                curRes = "2";
                currentResSlider = 0.5f;
                break;
            case "3":
                curRes = "3";
                currentResSlider = 1.0f;
                break;
            default:
                curRes = "1";   
                currentResSlider = 0.0f;
                break;        
        }
        volumeLevel = PlayerPrefs.GetFloat("volumeLevel");
        AudioListener.volume = volumeLevel;
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (curRes != screenres)
        {
            switch (screenres)
            {
                case "1":
                    Screen.SetResolution(1366, 768, fullscreen == 1);
                    resolution = "1366x768";
                    break;                
                case "2":
                    Screen.SetResolution(1600, 900, fullscreen == 1);
                    resolution = "1600x900";
                    break;
                case "3":
                    Screen.SetResolution(1920, 1080, fullscreen == 1);
                    resolution = "1920x1080";
                    break;
            }
            curRes = screenres;
        }
        if (AudioListener.volume != volumeLevel)
        {
            AudioListener.volume = volumeLevel;
        }

<<<<<<< HEAD
				inMenu = false;
				gameSettings = this.GetComponent<MainMenuScript> ();
				inventory = this.GetComponent<PlayerInventory> ();
				playerMotor = this.GetComponent<FPSWalkerEnhanced> ();
				if (PlayerPrefs.GetString ("fullscreen") == null) {
						PlayerPrefs.SetString ("fullscreen", "2");
				} else {
						switch (PlayerPrefs.GetString ("fullscreen")) {
						case "1":
								fullscreenBool = true;
								break;
						default:
								fullscreenBool = false;
								break;
						}
			
				}
		
				if (PlayerPrefs.GetString ("curRes") == null) {
						PlayerPrefs.SetString ("curRes", "1");
				} else {
						//gameSettings.curRes = PlayerPrefs.GetString ("curRes");
			
				}
		
				if (PlayerPrefs.GetFloat ("ambianceLevel") < 1) {
						PlayerPrefs.GetFloat ("ambianceLevel", 0.5f);
				} else {
						gameSettings.ambianceLevel = PlayerPrefs.GetFloat ("ambianceLevel");
			
				}
		
		}
	
		// Update is called once per frame
		void FixedUpdate ()
		{
				if (curRes != screenres) {
						switch (screenres) {
						case "1":
								Screen.SetResolution (1366, 768, fullscreenBool);
								resolution = "1366x768";
								break;
				
						case "2":
								Screen.SetResolution (1600, 900, fullscreenBool);
								resolution = "1600x900";
								break;
						case "3":
								Screen.SetResolution (1920, 1080, fullscreenBool);
								resolution = "1920x1080";
								break;
						}
						curRes = screenres;
				}
		if (Screen.fullScreen != InGameMenuScript.fullscreenBool) {
			Screen.fullScreen = InGameMenuScript.fullscreenBool;    
				}
				if (Input.GetButtonDown ("Cancel")) {
						inMenu = !inMenu;
						playerMotor.inMenu = !playerMotor.inMenu;
				}
				if (inMenu == true) {
						inventory.toggleInventory = false;
				}
=======
        if (Screen.fullScreen != (InGameMenuScript.fullscreen == 1))
        {
            Screen.fullScreen = InGameMenuScript.fullscreen == 1;    
        }
>>>>>>> 73090e954db80ca61d7bd6583bd35a3c13abd9f3

        if (Input.GetButtonDown("Cancel"))
        {
            inMenu = !inMenu;
            playerMotor.inMenu = !playerMotor.inMenu;
        }
        if (inMenu == true)
        {
            inventory.toggleInventory = false;
        }

    }

    void OnGUI()
    {
        if (settingsMenu == false && inMenu == true)
        {

            //Background for menu.
            GUI.Box(new Rect((Screen.width / 2) - 75, Screen.height / 2 - 100, 150, 225), "");
                        

            //Resume, Settings, Main Menu & Exit button is created.
            if (GUI.Button(new Rect((Screen.width / 2) - 30, Screen.height / 2 - 75, 60, 24), "Resume"))
            {
                playerMotor.inMenu = !playerMotor.inMenu;
                inMenu = false;
            }
            
            if (GUI.Button(new Rect((Screen.width / 2) - 35, Screen.height / 2 - 25, 70, 24), "Settings"))
            {
                settingsMenu = true;
            } 

            if (GUI.Button(new Rect((Screen.width / 2) - 40, Screen.height / 2 + 25, 80, 24), "Main Menu"))
            {
                Application.LoadLevel("menu");
            } 
            
            if (GUI.Button(new Rect((Screen.width / 2) - 30, Screen.height / 2 + 75, 60, 24), "Exit"))
            {
                Application.Quit();
            } 
            
        }
        

        //Settings menu for Ambience sound, Screen resolution and Fullscreen mode.
        if (settingsMenu == true)
        {
            GUI.Box(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300), "");
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300), "Sound");
            GUI.Label(new Rect(Screen.width / 2 - 145, Screen.height / 2 - 130, 300, 300), "Volume");
            GUI.Label(new Rect(Screen.width / 2 + 115, Screen.height / 2 - 130, 300, 300), ((int)(volumeLevel * 100)).ToString() + "%");
            volumeLevel = GUI.HorizontalSlider(new Rect(Screen.width / 2 - 80, Screen.height / 2 - 125, 190, 50), volumeLevel, 0.0f, 1.0f);
            
            //Video;
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 100, 300, 300), "Graphics");
            fullscreen = GUI.Toggle(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 80, 200, 30), fullscreen == 1, "Fullscreen") ? 1 : 0;
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 55, 300, 300), "Resolution");
            currentResSlider = GUI.HorizontalSlider(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 50, 180, 50), currentResSlider, 0.0f, 1.0f);
            GUI.Label(new Rect(Screen.width / 2 + 85, Screen.height / 2 - 55, 300, 300), resolution);
            if (currentResSlider <= 0.3f)
            {
                screenres = "1";
            } else if (currentResSlider > 0.3f && currentResSlider <= 0.6f)
            {
                screenres = "2";
            } else if (currentResSlider > 0.6f)
            {
                screenres = "3";
            }
            //Close button
            if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height / 2 + 110, 120, 30), "Close"))
            {
                settingsMenu = false;
                saveSettings(fullscreen, screenres, volumeLevel);
            }
        }
    }

    public static void saveSettings(int fullscreen, string screenres, float volumeLevel)
    {
        PlayerPrefs.SetInt("fullscreen", fullscreen);
        PlayerPrefs.SetString("screenres", screenres);
        PlayerPrefs.SetFloat("volumeLevel", volumeLevel);
        Debug.Log(PlayerPrefs.GetInt("fullscreen"));
        Debug.Log(PlayerPrefs.GetString("curRes"));
        Debug.Log(PlayerPrefs.GetFloat("volumeLevel"));
        
    }
}
#endif