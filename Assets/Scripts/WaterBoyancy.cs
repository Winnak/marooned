﻿using UnityEngine;
using System.Collections;

public class WaterBoyancy : MonoBehaviour 
{
    void OnTriggerStay(Collider other) 
    {
        if (other.gameObject.tag == "Player")
        {
            var player = other.gameObject.GetComponent<FPSWalkerEnhanced>();
            player.inWater = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            var player = other.gameObject.GetComponent<FPSWalkerEnhanced>();
            player.inWater = false;
        }
    }
}
