﻿using UnityEngine;
using System.Collections;

public class GalleonEvent : MonoBehaviour 
{
    //Timers
    [SerializeField]private float timer; //The time waited without the event
    [SerializeField]private float timerToEvent; //Time needed to wait for event to start
    [SerializeField]private float EventTimer; //The amount of time the event have run
    [SerializeField]private float timerToEventEnd; //The time untill event end

    private bool countToEvent;
    private bool eventTrigger; //The event trigger
    private bool createShip;

    public GameObject galleon; //The ships Gameobject
    private GameObject clone;

	// Use this for initialization
	void Start () {
        timer = 0;
        timerToEventEnd = 0;
        SetEventTimer();
        countToEvent = true;
        createShip = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (countToEvent)
        {
            timer += Time.deltaTime;
        }

       if (timer >= timerToEvent)
       {
           countToEvent = false;
           timer = 0;
           eventTrigger = true;
       }

        if (eventTrigger)
        {
            if (createShip)
            {
                var shippos = new Vector3(Random.Range(-10, 10), 6.3565f, Random.Range(-10, 10)).normalized;
                var hatpos = new Vector3(shippos.z, shippos.y, -shippos.x) * 2000; 
                clone = (GameObject)Instantiate(galleon, shippos * 2000, Quaternion.identity);
                clone.transform.position = clone.transform.position.NUp() + Vector3.up * 28;
                clone.transform.rotation = Quaternion.LookRotation(
                    Vector3.RotateTowards(this.transform.position.NUp(), hatpos.NUp(), 100, 100));
                clone.transform.Rotate(Vector3.up, 45);
                createShip = false;
            }

            timerToEventEnd += Time.deltaTime;

            if (timerToEventEnd >= EventTimer)
            {
                eventTrigger = false;
                countToEvent = true;
                timerToEventEnd = 0;
                SetEventTimer();
                createShip = true;
                Destroy(clone);
            }
        }
	}

    public void SetEventTimer()
    {
        timerToEvent = Random.Range(10.0f, 50.0f);
        EventTimer = 100f;

    }
}
