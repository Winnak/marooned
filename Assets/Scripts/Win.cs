﻿using UnityEngine;
using System.Collections;

public class Win : MonoBehaviour 
{
    private bool litFire;
    private GameObject[] campfires;
    private bool fireDetected;
    public int detectionRange = 400;
    public GameObject fader;


    void Start()
    {
        fader = GameObject.FindGameObjectWithTag("Fader");

    }
	void FixedUpdate () 
    {
        /*
        if (GameObject.Find("CampFireLit"))
            litFire = true;
        else
            litFire = false;

        if (litFire == true)
            Debug.Log("You win");
         */
	}

    void Update()
    {
        transform.Translate(Vector3.forward * 15 * Time.deltaTime);
        if (FireDetected())
        {
            Debug.Log("I found you");
            fader.GetComponent<Screenfader>().hasWon = true;
        }
    }

    bool FireDetected()
    {
        campfires = GameObject.FindGameObjectsWithTag("Campfire");

        foreach (GameObject campfire in campfires)
        {
            if (Vector3.Distance(campfire.transform.position, this.transform.position) < detectionRange)
                return true;
        }

        return false;

    }
}
