﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CraftingRecipe : MonoBehaviour 
{
    //List containing input items for crafting
    public List<Item> InputItems = new List<Item>();

    //List containing output items for crafting
    public List<Item> Outputs = new List<Item>();

    //Not done yet
}
